package com.api.xmlshitwork.service;

import com.api.xmlshitwork.dto.BulkBillDeskExternalResponseDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author Salman aka theFreakingMind
 * @date 23/12/22
 */

@Service
@RequiredArgsConstructor
public class DataService {

   private final WebClient client;
   private final XmlMapper xmlMapper;

   public BulkBillDeskExternalResponseDto xmlResponseData(String data) throws JsonProcessingException {
      String responseData = client
              .get()
              .uri("https://uat.billdesk.com/ecom/ECOM2ReqHandler?msg=" + data)
              .retrieve()
              .bodyToMono(String.class)
              .block();

      BulkBillDeskExternalResponseDto responseDataManager = xmlMapper
              .readValue(responseData, BulkBillDeskExternalResponseDto.class);
      return responseDataManager;
   }
}
