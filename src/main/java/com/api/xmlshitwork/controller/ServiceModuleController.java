package com.api.xmlshitwork.controller;

import com.api.xmlshitwork.dto.BulkBillDeskExternalResponseDto;
import com.api.xmlshitwork.dto.People;
import com.api.xmlshitwork.service.DataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * @author Salman aka theFreakingMind
 * @date 24/12/22
 */
@RestController
@RequestMapping("/api/service/")
@RequiredArgsConstructor
public class ServiceModuleController {

   private final DataService dataService;

   @RequestMapping(value = "/value", method = RequestMethod.POST, produces = MediaType.TEXT_XML_VALUE)
   public BulkBillDeskExternalResponseDto data(@RequestParam("data") String data) throws IOException, ParserConfigurationException, SAXException {
      return dataService.xmlResponseData(data);

   }
}
