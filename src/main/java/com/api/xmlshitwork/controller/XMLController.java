package com.api.xmlshitwork.controller;

import com.api.xmlshitwork.dto.BullShitRequest;
import com.api.xmlshitwork.dto.People;
import com.api.xmlshitwork.service.XMLService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Salman aka theFreakingMind
 * @date 23/12/22
 */

@RestController
@RequiredArgsConstructor
public class XMLController {

   private final XMLService xmlService;


   @RequestMapping(value = "/api/shit", method = RequestMethod.POST, produces = MediaType.TEXT_XML_VALUE)
   public ResponseEntity<List<People>> getPeopleData(@RequestParam("data")String data){
      System.out.println(data);
      return ResponseEntity.ok(xmlService.peopleList(data));

   }
}
