package com.api.xmlshitwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmlShitworkApplication {

   public static void main(String[] args) {
      SpringApplication.run(XmlShitworkApplication.class, args);
   }

}
