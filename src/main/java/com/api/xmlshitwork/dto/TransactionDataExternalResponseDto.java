package com.api.xmlshitwork.dto;

/**
 * @author Salman aka theFreakingMind
 * @date 25/12/22
 */
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "TXNDATA")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionDataExternalResponseDto {
   @XmlElement(name="TXNSUMMARY")
   public TransactionSummaryExternalResponseDto TXNSUMMARY;

   @XmlElement(name="RECORD")
   public RecordExternalResponseDto RECORD;

   @Override
   public String toString() {
      return "TransactionDataExternalResponseDto{" +
              "TXNSUMMARY=" + TXNSUMMARY +
              ", RECORD=" + RECORD +
              '}';
   }
}

