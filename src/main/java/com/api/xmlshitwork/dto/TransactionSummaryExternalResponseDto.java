package com.api.xmlshitwork.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TXNSUMMARY")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionSummaryExternalResponseDto {

    @XmlElement(name="PGMERCID")
    public String PGMERCID;

    @XmlElement(name="RECORDS")
    public Integer RECORDS;

    @XmlElement(name="PGCUSTOMERID")
    public String PGCUSTOMERID;

    @XmlElement(name="AMOUNT")
    public String AMOUNT;

    @XmlElement(name="STATUSDESC")
    public String STATUSDESC;

    @XmlElement(name="STATUSCODE")
    public Integer STATUSCODE;

    @XmlElement(name="FILLER1")
    public String FILLER1;

    @XmlElement(name="FILLER2")
    public String FILLER2;

    @XmlElement(name="FILLER3")
    public String FILLER3;

    @Override
    public String toString() {
        return "TransactionSummaryExternalResponseDto{" +
                "PGMERCID='" + PGMERCID + '\'' +
                ", RECORDS=" + RECORDS +
                ", PGCUSTOMERID='" + PGCUSTOMERID + '\'' +
                ", AMOUNT='" + AMOUNT + '\'' +
                ", STATUSDESC='" + STATUSDESC + '\'' +
                ", STATUSCODE=" + STATUSCODE +
                ", FILLER1='" + FILLER1 + '\'' +
                ", FILLER2='" + FILLER2 + '\'' +
                ", FILLER3='" + FILLER3 + '\'' +
                '}';
    }
}
