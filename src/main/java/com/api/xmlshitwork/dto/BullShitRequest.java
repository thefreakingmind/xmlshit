package com.api.xmlshitwork.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Salman aka theFreakingMind
 * @date 23/12/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class BullShitRequest {

   private String nameContainsValue;
   private int age;
}
