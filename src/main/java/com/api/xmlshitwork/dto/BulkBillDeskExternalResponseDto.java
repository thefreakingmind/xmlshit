package com.api.xmlshitwork.dto;

/**
 * @author Salman aka theFreakingMind
 * @date 25/12/22
 */
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RESPONSE")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class BulkBillDeskExternalResponseDto {

   @XmlElement(name="TXNDATA")
   public TransactionDataExternalResponseDto TXNDATA;

   @XmlElement(name="CHECKSUM")
   public String CHECKSUM;

   @Override
   public String toString() {
      return "BulkBillDeskExternalResponseDto{" +
              "TXNDATA=" + TXNDATA +
              ", CHECKSUM='" + CHECKSUM + '\'' +
              '}';
   }
}

