package com.api.xmlshitwork.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "RECORD")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class RecordExternalResponseDto {
    @XmlAttribute(name = "ID")
    public int ID;

    @XmlElement(name="CUSTOMERID")
    public String CUSTOMERID;

    @XmlElement(name="ECOMTXNID")
    public String ECOMTXNID;

    @Override
    public String toString() {
        return "RecordExternalResponseDto{" +
                "ID=" + ID +
                ", CUSTOMERID='" + CUSTOMERID + '\'' +
                ", ECOMTXNID='" + ECOMTXNID + '\'' +
                '}';
    }
}
