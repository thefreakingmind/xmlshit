package com.api.xmlshitwork.dto;

import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Salman aka theFreakingMind
 * @date 23/12/22
 */
@Data
@AllArgsConstructor
@XmlRootElement
@NoArgsConstructor
public class People {
   int id;
   String name;
   int age;
}

