package com.api.xmlshitwork.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.util.MimeType;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * @author Salman aka theFreakingMind
 * @date 23/12/22
 */
@Configuration
public class Config {

   @Bean
   public XmlMapper xmlMapper(){
      return new XmlMapper();

   }


   private ExchangeFilterFunction contentTypeChanger() {
      return ExchangeFilterFunction.ofResponseProcessor(dataLayer ->
              Mono.just(ClientResponse
                      .from(dataLayer)
                      .headers(headers -> headers.remove(HttpHeaders.CONTENT_TYPE))
                      .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE)
                      .body(dataLayer.body(BodyExtractors.toDataBuffers()) )
                      .build()));
   }

   @Bean
   public WebClient client() {
      return WebClient
              .builder()
              .filter(contentTypeChanger())
              .build();

   }
}
